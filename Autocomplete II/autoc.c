#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
void sortc(double ar[], int f, int l){
    if(f<l){
        //prvni prvek
        double pivot = ar[f];
        int a = f, b = l;
        //cyklus dokud prvky mensi nez pivot nebudou mit mensi index nez prvky vetsi(nebo rovny) nez pivot
        while(a<=b){
        	//HLEDAM PRVEK(od nulteho prvku), ktery je vetsi nebo roven pivotu. 
            while(ar[a]>(pivot)){a+=3;}

            //HLEDAM PRVEK(od konce), ktery je mensi nebo roven pivotu. 
            while(ar[b]<(pivot)){b-=3;}

            //IF prvek vetsi [a] nez pivot ma mensi index <<<< nez prvek mensi [b] nez pivot --> vymena
            if(a<=b){
                double tmp0 = ar[a];
                double tmp1 = ar[a+1];
                double tmp2 = ar[a+2];
              
                ar[a] = ar[b];
                ar[a+1] = ar[b+1];
                ar[a+2] = ar[b+2];
              
             
                ar[b] = tmp0;
                ar[b+1] = tmp1;
                ar[b+2] = tmp2;
             
                a+=3;
                b-=3;
            }
 

        } 
        sortc(ar, f, b); //leva cast
        sortc(ar, a, l); //prava cast
    }
}
int main(void){
	printf("%s\n", "Casto hledane fraze:");

	int max = 20;
	int maxPl = 1000;

	char *phrases = (char *)malloc(sizeof(char) * max * maxPl); 
	double *copos = (double *)malloc(sizeof(double) * 3 * max); 

	char *line = NULL;
	size_t len=0, l=0;
	int ll = 0;
	

	int t = 0;
	int col = 0;
	int notok = 0;
	
  	char znak;

	//Nacteni vstupu
	while(1){
		l += ll - col;
	    if(t>=(3*max)){
            max *= 3;
            phrases = (char *)realloc(phrases, sizeof(char) * max * maxPl); 
            copos = (double *)realloc(copos, sizeof(double) * 3 * max);
        }
		
		ll = getline(&line, &len, stdin);
		

		if(maxPl < ll){
		    maxPl = ll;
		    phrases = (char *)realloc(phrases, sizeof(char) * max * maxPl); 
		}

		if(*line=='\n'){
		    break;
		}
		
		if((ll==EOF)||(sscanf(line, "%lf%c%n ", copos+t, &znak ,&col)!=2)||(znak!=':')){
			notok = 1;
			break;
		}
     
		*(copos + t + 1)=l;
		*(copos + t + 2)=l+ll-col; 
		
		for(int k = 0; k < ll-col; k++){
			*(phrases+l+k)=*(line+col+k);
		}

		t+=3;
	}
	
  
	if((t==0)||(notok == 1)){
		printf("%s\n", "Nespravny vstup.");
	}

	else{ 
		//sort od nejvetsiho koeficientu
		sortc(copos, 0, t-3);
	
		int pl;
		int count;
		int match;
		int k;
		int *range = (int *)malloc(sizeof(int) * 2 * t); 
		printf("%s\n", "Hledani:");
		while(1){
			pl = getline(&line, &len, stdin);
			//printf("pl ->>>>>>>>>>> %d", pl);
			if(pl==EOF){
				break;
			}
			else{
				k = 0;
				count = 0;
				int m;
				int d=0;
				int q =0;
				while(k<t){
						//printf("%d", k);
						match = 0;
						m = *(copos+k+1);
						q=0;
						for(int i=0; i<(*(copos+k+2)-*(copos+k+1)); i++){
							
							if(q<(pl-1)){
								//printf("%c == %c\n", *(line+q),*(phrases+m+i) );
								if(*(line+q)==*(phrases+m+i)){
									
									match++;
									q++;

								}else if(tolower(*(line+q))==tolower(*(phrases+m+i))){
									match++;
									q++;
								}
								
								else{
									if(match!=0){
										i-=match;
									}
									match = 0;
									q=0;
								}
							}else{break;}
						}
						
						if(match==(pl-1)){
							count++;
							if(count<=50){
								*(range+d)=*(copos+k+1);
								*(range+d+1)=*(copos+k+2);
								d+=2;
							}
						}	
					k+=3;
				}
				
				printf("%s%d\n", "Nalezeno: ", count);
				for (int i = 0; i < d; i+=2){
					printf("> ");
					for(int j = *(range+i); j < *(range+i+1); j++){
						printf("%c", *(phrases+j));
					}
				}
				
			}
			
		}
		free(range);
	}

	free(copos);
	free(line);
	free(phrases);
	

	return 0;
}