#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LIST_BY_YEAR       0
#define LIST_BY_TYPE       1

#define TYPE_MAX           100
#define SETUP_MAX          100

typedef struct TEngine
{
  struct TEngine * m_Next;
  struct TEngine * m_Prev;
  int              m_Year;
  char             m_Type  [ TYPE_MAX ];
  int              m_Setup [ SETUP_MAX ];
} TENGINE;

typedef struct TArchive
{
  struct TArchive * m_Next;
  struct TArchive * m_Prev;
  TENGINE         * m_Engines;
} TARCHIVE;

TENGINE          * createEngine                            ( const char      * type,
                                                             int               year )
{
  TENGINE * res = (TENGINE *) malloc ( sizeof  (*res ) );
  res -> m_Next = NULL;
  res -> m_Prev = NULL;
  res -> m_Year = year;
  strncpy ( res -> m_Type, type, sizeof ( res -> m_Type ) );
  for ( int i = 0; i < SETUP_MAX; i ++ )
    res -> m_Setup[i] = 0;
  return res;
}
#endif /* __PROGTEST__ */

TARCHIVE         * AddEngine                               ( TARCHIVE        * list,
                                                             int               listBy,
                                                             TENGINE         * engine )
{
  //Empty list
if(engine==NULL){return list;}
if(list==NULL){
  list=(TARCHIVE *)calloc(1, sizeof(TARCHIVE));
  list->m_Next = NULL;
  list-> m_Prev = NULL;
  list->m_Engines=engine;
  return list;
}


TARCHIVE * nextList = list, * tmpList = list;
int ex=0;
if(listBy == 0){
  while(nextList!=NULL){
    if(engine->m_Year < nextList->m_Engines->m_Year){break;}
    if(engine->m_Year == nextList->m_Engines->m_Year){ex=1;break;}
    tmpList = nextList;
    nextList = nextList -> m_Next;
  }
}
else{
  while(nextList!=NULL){
    if(strcmp(engine->m_Type, nextList->m_Engines->m_Type)<0){break;}
    if(strcmp(engine->m_Type, nextList->m_Engines->m_Type)==0){ex=1;break;}
    tmpList = nextList;
    nextList = nextList -> m_Next;
  }
}

if(ex==0){
    TARCHIVE * newList = (TARCHIVE *)calloc(1,sizeof(TARCHIVE));
    newList -> m_Prev = NULL;
    newList -> m_Next = NULL;
    newList->m_Engines=engine;
    if(nextList == list){
      newList->m_Next=list;
      list -> m_Prev = newList;
      return newList;
    }
    else if(nextList==NULL){
      tmpList -> m_Next = newList;
      newList -> m_Prev = tmpList;
      return list;
    }
    else{
     TARCHIVE * tmptmpListNext = tmpList -> m_Next;
      newList -> m_Next = tmptmpListNext;
      tmptmpListNext -> m_Prev = newList;
      newList -> m_Prev = tmpList;
      tmpList -> m_Next = newList;
      return list;
    }
  }else{
      TENGINE * nextEngine = nextList -> m_Engines, * tmpEngine = nextList -> m_Engines;
      
      if(listBy == 0){
        while(nextEngine!=NULL){
            if(strcmp(engine->m_Type, nextEngine->m_Type) <= 0){
              break;
            }
            tmpEngine = nextEngine;
            nextEngine= nextEngine -> m_Next;
        }
      }
      else{
         while(nextEngine!=NULL){
          if(engine->m_Year <= nextEngine->m_Year){
            break;
          }
          tmpEngine = nextEngine;
          nextEngine= nextEngine -> m_Next;
        }
      }
      if(nextEngine == nextList -> m_Engines){
        TENGINE * fe =  nextList -> m_Engines;
        engine -> m_Next = fe;
        fe -> m_Prev = engine;
        nextList -> m_Engines = engine;
        return list;
      }
      if(nextEngine == NULL){
          tmpEngine -> m_Next = engine;
          engine -> m_Prev = tmpEngine;
          return list;
      }
      else{
        engine -> m_Next = nextEngine;
        nextEngine -> m_Prev = engine;
        engine -> m_Prev = tmpEngine;
        tmpEngine -> m_Next = engine;
        return list;
      }
      
    }

 

}
void               DelArchive                              ( TARCHIVE        * list )
{
  if(list != NULL){
    DelArchive(list -> m_Next);
    TENGINE * engine = list -> m_Engines, * tmpNextEngine;
    while(engine != NULL){
        tmpNextEngine = engine -> m_Next;
        free(engine);
        engine = tmpNextEngine;
    }
    free(list);
  }
}
TARCHIVE         * ReorderArchive                          ( TARCHIVE        * list,
                                                             int               listBy )
{
  if(list==NULL){return list;}
  TARCHIVE * tmpNext = list;
  TENGINE * tmpOneNext, *tmpOne = list -> m_Engines, *tmpOnePrev;
  while(tmpNext!=NULL){ 
    tmpOneNext = tmpNext -> m_Engines;
    while(tmpOneNext!=NULL){
      tmpOne = tmpOneNext;
      tmpOneNext = tmpOneNext -> m_Next;
    }

    tmpNext = tmpNext -> m_Next;
    
    if(tmpNext!=NULL){ tmpOnePrev=tmpNext -> m_Engines;tmpOne -> m_Next = tmpNext -> m_Engines;  tmpOnePrev -> m_Prev = tmpOne; }
  
  }


  
  TENGINE * engineF = list -> m_Engines, * engineS;
  TENGINE * engineSNext = list -> m_Engines, * engineFBefore , * engineSBefore , * tmpEng;
  engineFBefore = engineF;
  while(engineF!=NULL){
    engineSBefore = engineF;
    engineS = engineF -> m_Next;
   
    while(engineS!=NULL){

      if(listBy == 0){
        if(engineS -> m_Year <= engineF -> m_Year){
          int ok = 1;
              if(engineS -> m_Year == engineF -> m_Year && strcmp(engineF -> m_Type,engineS -> m_Type) < 0){
                ok = 0;
              }
              if(ok==1){
                    engineSNext = engineS -> m_Next;
                  if(list -> m_Engines!=engineF){engineFBefore->m_Next=engineS;engineS->m_Prev=engineFBefore;}else{list -> m_Engines = engineS; engineS->m_Prev=NULL;}
                 
                  if(engineSBefore!=engineF){
                    engineSBefore -> m_Next = engineF;
                    engineF->m_Prev=engineSBefore;
                    engineS -> m_Next = engineF -> m_Next;
                  }
                  else{
                    engineS->m_Next=engineF;
                    engineF -> m_Prev =engineS;
                  }
                  engineF -> m_Next = engineSNext;
                  if(engineSNext!=NULL){engineSNext -> m_Prev = engineF;}
                  tmpEng = engineS;
                  engineS = engineF;
                  engineF = tmpEng;
              }
        }
      }else{ 

        if(strcmp(engineS -> m_Type, engineF -> m_Type) <= 0){
            int ok = 1;
              if(strcmp(engineS -> m_Type, engineF -> m_Type) == 0 && engineS -> m_Year > engineF -> m_Year){
                ok = 0;
              }
              if(ok==1){
                 engineSNext = engineS -> m_Next;
                  if(list -> m_Engines!=engineF){engineFBefore->m_Next=engineS;engineS->m_Prev=engineFBefore;}else{list -> m_Engines = engineS; engineS->m_Prev=NULL;}
                 
                  if(engineSBefore!=engineF){
                    engineSBefore -> m_Next = engineF;
                    engineF->m_Prev=engineSBefore;
                    engineS -> m_Next = engineF -> m_Next;
                  }
                  else{
                    engineS->m_Next=engineF;
                    engineF -> m_Prev =engineS;
                  }
                  engineF -> m_Next = engineSNext;
                    if(engineSNext!=NULL){engineSNext -> m_Prev = engineF;}
                  tmpEng = engineS;
                  engineS = engineF;
                  engineF = tmpEng;
            }
        }
      }
      
      engineSBefore = engineS;
      engineS = engineS -> m_Next;
    }

    engineFBefore = engineF;
    engineF = engineF -> m_Next;
  }

 

  TENGINE * teng = list -> m_Engines, * mem, * prevEng;
  TARCHIVE * arch = list;
  while(teng != NULL && teng -> m_Next != NULL){

    mem = teng -> m_Next;

    if(listBy == 0){

      if((teng -> m_Year != teng -> m_Next -> m_Year)){
        if(arch -> m_Next != NULL){arch = arch -> m_Next;}else{
          TARCHIVE * newArch = (TARCHIVE *) calloc ( 1,sizeof  (*newArch ) );

          arch -> m_Next = newArch;
          newArch -> m_Next = NULL;
          newArch -> m_Prev = arch;
          arch = arch -> m_Next;
        }
        prevEng = teng -> m_Next;
        prevEng -> m_Prev = NULL;
        arch -> m_Engines = teng -> m_Next;
        teng -> m_Next = NULL;
        
      }
    }
    else{
      if(strcmp(teng -> m_Type, teng -> m_Next -> m_Type) != 0){
        if(arch -> m_Next != NULL){arch = arch -> m_Next;}else{
          TARCHIVE * newArch = (TARCHIVE *) calloc (1, sizeof  (*newArch ) );

            arch -> m_Next = newArch;
          newArch -> m_Next = NULL;
          newArch -> m_Prev = arch;
          arch = arch -> m_Next;
        }
        prevEng = teng -> m_Next;
        prevEng -> m_Prev = NULL;
        arch -> m_Engines = teng -> m_Next;
        teng -> m_Next = NULL;
        
      }
    }
    teng = mem;
  }
  if(arch->m_Next!=NULL){
    TARCHIVE * tarch = arch->m_Next, * farch;
    while(tarch!=NULL){
        farch = tarch -> m_Next;
        free(tarch);
        tarch = farch;
    }
    
    arch->m_Next=NULL;
    
  }


  return list;
    
}
#ifndef __PROGTEST__
int                main                                    ( int               argc,
                                                             char            * argv [] )
{


  return 0;
}
#endif /* __PROGTEST__ */
