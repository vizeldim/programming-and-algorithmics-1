#include <stdio.h>
#include <math.h>
#include <float.h>
#include <string.h>
double t1[3],t2[3];
double st_rad(double stupen) {
	double rad = (stupen / 180.0) * ((double) M_PI);
   	return rad;
}
int nacti_trojuhelnik(double *x, double *y, double *z){
	double a=0,b=0,c=0;
	char v1=' ',v2=' ',v3=' ';
	if( ((scanf(" %c%c%c %lf %lf %lf", &v1, &v2, &v3, &a, &b, &c))!=6)||
		(a<=0)||(b<=0)||(c<=0)||
		!( ((v1=='S')&&(v2=='S')&&(v3=='S')) ||
		((v1=='S')&&(v2=='U')&&(v3=='S')) ||
		((v1=='U')&&(v2=='S')&&(v3=='U')) )
		){
		printf("%s\n", "Nespravny vstup.");
		return 0;
	}
	else{
		int podminka = 1;
		if((v1=='S')&&(v2=='U')){
			if(b>=180){printf("%s\n", "Nespravny vstup."); podminka = 0;}
			else{b=sqrt(pow(a,2) + pow(c,2) - 2*a*c*cos(st_rad(b)));}

		}
		else if((v1=='U')&&(v2=='S')){
			if((a>=180)||(c>=180) ){printf("%s\n", "Nespravny vstup."); podminka = 0;}
			else{
				double gama;
				gama = 180 - a - c;
				if((fabs(gama)) <= DBL_EPSILON * 100 * (a+c)){
					printf("%s\n", "Vstup netvori trojuhelnik.");
					podminka=0;
				}else{
					a=b*(sin(st_rad(a))/sin(st_rad(gama)));
					c=b*(sin(st_rad(c))/sin(st_rad(gama)));
				}
				
			}
		}
		if(podminka==1){
			if( (a>=(b+c - DBL_EPSILON * 100 * (b+c)))||
				(b>=(a+c - DBL_EPSILON * 100 * (b+c)))||
				(c>=(a+b - DBL_EPSILON * 100 * (a+b)))){
				printf("%s\n", "Vstup netvori trojuhelnik.");
				return 0;
			}
			else{
				*x = a;
				*y = b;
				*z = c;
				return 1;
			}
		}
		else{return 0;}
	}
}
double * serad(double p[]){
	for(int i=0; i<3; i++){
		for(int j=1; j<3; j++){
			if(p[j-1]>p[j]){double s = p[j]; p[j] = p[j-1]; p[j-1]=s;}
		}
	}
	return p;
}
int podobnost_shoda(double tr1[], double tr2[]){
	tr1 = serad(tr1);
	tr2 = serad(tr2);
	double kfc[3];
	for(int i=0;i<3;i++){
		if(tr1[i]>tr2[i]){kfc[i]=(tr1[i]/tr2[i])*1.0;}
		else{kfc[i]=(tr2[i]/tr1[i])*1.0;}
	}

	if(  ( fabs(kfc[0] - kfc[1]) < DBL_EPSILON * 1000 * fabs(kfc[0]) )&&
	     ( fabs(kfc[1] - kfc[2]) < DBL_EPSILON * 1000 * fabs(kfc[2]) )
	   ){
		if(fabs(kfc[0]-1) < DBL_EPSILON * 100){return 1;}else{return 2;}
	}else{return 0;}
}

int main(void){
	printf("%s\n", "Trojuhelnik #1:" );
	if(nacti_trojuhelnik(&t1[0],&t1[1],&t1[2])){
		printf("%s\n", "Trojuhelnik #2:" );
		if(nacti_trojuhelnik(&t2[0],&t2[1],&t2[2])){
			int ps = (podobnost_shoda(t1, t2));
			if(ps==1){printf("%s\n", "Trojuhelniky jsou shodne.");}
			else if(ps==2){printf("%s\n", "Trojuhelniky nejsou shodne, ale jsou podobne.");}
			else{printf("%s\n", "Trojuhelniky nejsou shodne ani podobne.");}
		}
	}
	return 0;
}