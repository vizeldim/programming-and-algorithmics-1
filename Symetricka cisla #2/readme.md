Úkolem je vytvořit program, který bude počítat čísla, která mají symetrický zápis v zadané číselné soustavě. Jedná se o rozšíření jednodušší úlohy, rozšíření spočívá v možnosti udat základ číselné soustavy. Doporučujeme nejprve řešit úlohu jednodušší a po jejím úspěšném odevzdání program rozšířit.

Vstupem programu je posloupnost příkazů k hledání. Zadávání je ukončené dosažením konce vstupu (EOF). Každý příkaz se skládá ze čtveřice údajů X R LO HI. První znak udává, zda chceme nalezená symetrická čísla všechna vypsat (na vstupu bude znak l - list) nebo zda je chceme pouze spočítat (na vstupu bude znak c - count). Za znakem následuje základ číselné soustavy (v této soustavě chceme hledat symetrická čísla) a konečně dolní a horní mez prohledávaného intervalu - čísla LO a HI. Prohledává se uzavřený interval hodnot, tedy do prohledávání jsou zahrnuta i obě čísla LO a HI.

Výstupem programu je odpověď na každý vstupní příkaz. Odpovědí na příkaz l je seznam nalezených čísel, formát je zřejmý z ukázky. Odpovědí na příkaz c je počet nalezených čísel.

Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). Za chybu považujte:

neznámý příkaz (ani l ani c),
nečíselné zadání základu soustavy, základ soustavy mimo interval <2;36>,
nečíselné zadání intervalu nebo chybějící meze LO a HI,
dolní mez je záporná,
dolní mez je větší než horní mez.
Ukázka práce programu:
Vstupni intervaly:
l 2 0 20
0 = 0 (2)
1 = 1 (2)
3 = 11 (2)
5 = 101 (2)
7 = 111 (2)
9 = 1001 (2)
15 = 1111 (2)
17 = 10001 (2)
c 2 0 20
Celkem: 8
l 3 0 40
0 = 0 (3)
1 = 1 (3)
2 = 2 (3)
4 = 11 (3)
8 = 22 (3)
10 = 101 (3)
13 = 111 (3)
16 = 121 (3)
20 = 202 (3)
23 = 212 (3)
26 = 222 (3)
28 = 1001 (3)
40 = 1111 (3)
l 5 0 50
0 = 0 (5)
1 = 1 (5)
2 = 2 (5)
3 = 3 (5)
4 = 4 (5)
6 = 11 (5)
12 = 22 (5)
18 = 33 (5)
24 = 44 (5)
26 = 101 (5)
31 = 111 (5)
36 = 121 (5)
41 = 131 (5)
46 = 141 (5)
l 17 1918 2019
1927 = 6b6 (17)
1944 = 6c6 (17)
1961 = 6d6 (17)
1978 = 6e6 (17)
1995 = 6f6 (17)
2012 = 6g6 (17)
c 31 38 12345
Celkem: 396
Vstupni intervaly:
c 6 1234567 7654321
Celkem: 4955
l 16 255 255
255 = ff (16)
x 18 25 97
Nespravny vstup.
Vstupni intervaly:
c 100 90 100
Nespravny vstup.
Vstupni intervaly:
l radix 10 20
Nespravny vstup.
