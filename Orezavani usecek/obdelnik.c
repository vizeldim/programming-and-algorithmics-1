#ifndef __PROGTEST__
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#endif /* __PROGTEST__ */

int                clipLine                                ( double            rx1,
                                                             double            ry1,
                                                             double            rx2,
                                                             double            ry2,
                                                             double          * ax,
                                                             double          * ay,
                                                             double          * bx,
                                                             double          * by )
{
/* todo */
  double xa = *ax, xb = *bx, ya = *ay, yb = *by;
  double x, y; 
  if(rx1 > rx2){
    double tmp = rx1;
    rx1 = rx2;
    rx2 = tmp;
  }
  if(ry1 > ry2){
    double tmp = ry1;
    ry1 = ry2;
    ry2 = tmp;
  }
  bool inside=false;
  bool topA, bottomA, leftA, rightA;
  bool topB, bottomB, leftB, rightB;
  bool outA, inA;
  bool outB, inB;
  bool sameOutAB, inAB;
  bool top, bottom, left, right;
  while(1){
    //POLOHA A
    topA = (ya > ry2);
    bottomA = (ya < ry1);
    leftA = (xa < rx1);
    rightA = (xa > rx2);

    outA = (bottomA || topA || leftA || rightA);
    inA = !outA; 
    
    //POLOHA B
    topB = (yb > ry2);
    bottomB = (yb < ry1);
    leftB = (xb < rx1);
    rightB = (xb > rx2);

    outB = (bottomB || topB || leftB || rightB);
    inB = !outB;

    //Oba body vne na stejne strane
    sameOutAB = (bottomA && bottomB) || (topA && topB) || (leftA && leftB) || (rightA && rightB);
    //Oba body uvnitr obdelniku
    inAB = inA && inB;
    if(sameOutAB){
      break;

    }
    else if(inAB){
      inside = true;
      break;
    }
    else{
      if (!inA){x = xa; y = ya;// printf("A je vne\n"); 
    }
      else{x = xb; y = yb;// printf("A je uvnitr\n");
    }

      //Poloha meneneho bodu
      top = (y > ry2);
      bottom = (y < ry1);
      left = (x < rx1);
      right = (x > rx2);

      if(top){
        x = xa + ((xb - xa)/(yb - ya))*(ry2 - ya) ;
       
        y = ry2; 
       // printf("TOP%lf > %lf\n", y, ry2);break;
      }
      else if(bottom){
           // printf("BOTTOM%lf < %lf\n", y, ry1);
        x = xa + ((xb - xa)/(yb - ya))*(ry1 - ya) ;
       
        y = ry1; 
      // printf("BOTTOM%lf < %lf\n", y, ry1);
      }
      else if(right){
        x = rx2; 
        y = ya + ((yb - ya)/(xb - xa))*(rx2 - xa) ; 
       
       //printf("RIGHT%lf > %lf\n", x, rx2);break;
      }
      else if(left){
     //  printf("LEFT%lf < %lf\n", x, rx1);break;
        x = rx1; 
        y = ya + ((yb - ya)/(xb - xa))*(rx1 - xa) ; 
        
       // printf("LEFT%.400lf < %.400lf\n", x, rx1);
      }

      if(abs(x-rx1)<= DBL_EPSILON*100*(x+rx1) ){x = rx1;}
      if(abs(x-rx2)<= DBL_EPSILON*100*(x+rx2) ){x = rx2;}
      if(abs(y-ry1)<= DBL_EPSILON*100*(y+ry1) ){y = ry1;}
      if(abs(y-ry2)<= DBL_EPSILON*100*(y+ry2) ){y = ry2;}
      if (!inA){xa = x; ya = y;}else{xb = x; yb = y;}
    }
  }

  //Pokud oba body uvnitr (rovnou nebo po posunuti)
  if(inside){
    *ax = xa;
    *ay = ya;
    *bx = xb;
    *by = yb;
   // printf("Vrati 1 %lf %lf %lf %lf\n", xa, ya, xb, yb);
    return 1;
  }else{
  //printf("Vrati 0 %lf %lf %lf %lf\n", xa, ya, xb, yb);
  return 0;}
}

#ifndef __PROGTEST__
int                almostEqual                             ( double            x,
                                                             double            y )
{
  /* todo */
  if((x - y) <= DBL_EPSILON * 100 * (x+y)){
    return 1;
  }else{
    return 1;
  }
return 1;
}

int                main                                    ( void )
{
  double x1, y1, x2, y2;

  x1 = 60;
  y1 = 40;
  x2 = 70;
  y2 = 50;
  assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 60 )
           && almostEqual ( y1, 40 )
           && almostEqual ( x2, 70 )
           && almostEqual ( y2, 50 ) );
 x1 = 0;
  y1 = 50;
  x2 = 20;
  y2 = 30;
  assert ( clipLine ( 90, 100, 10, 20, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 10 )
           && almostEqual ( y1, 40 )
           && almostEqual ( x2, 20 )
           && almostEqual ( y2, 30 ) );

 
  x1 = 0;
  y1 = 30;
  x2 = 120;
  y2 = 150;
  assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 10 )
           && almostEqual ( y1, 40 )
           && almostEqual ( x2, 70 )
           && almostEqual ( y2, 100 ) );

  x1 = -10;
  y1 = -10;
  x2 = -20;
  y2 = -20;
  assert ( ! clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 ) );

  x1 = 0;
  y1 = 30;
  x2 = 20;
  y2 = 10;
  assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 10 )
           && almostEqual ( y1, 20 )
           && almostEqual ( x2, 10 )
           && almostEqual ( y2, 20 ) );

 x1 = 0;
  y1 = 0.3553;
  x2 = 10.45;
  y2 = 0;
  assert ( clipLine ( 0.95, 0.323, 1, 1, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 0.95 )
           && almostEqual ( y1, 0.323 )
           && almostEqual ( x2, 0.95 )
           && almostEqual ( y2, 0.323 ) );


  x1 = 0;
  y1 = 0.6083;
  x2 =3.047;
  y2 = 0;
 assert ( clipLine ( 0.277, 0.553, 1, 1, &x1, &y1, &x2, &y2)
 && almostEqual ( x1, 0.277 )
           && almostEqual ( y1, 0.553 )
           && almostEqual ( x2, 0.277 )
           && almostEqual ( y2, 0.553 ) );
  //printf("%lf %lf %lf %lf\n", x1, y1, x2, y2);
  x1 =  -2412;
  y1 =  -2721;
  x2 =-654;
  y2 = -885;
 assert ( clipLine (  -654, 33, 225, -885,  &x1, &y1, &x2, &y2)
 && almostEqual ( x1, -654)
           && almostEqual ( y1, -885)
           && almostEqual ( x2, -654)
           && almostEqual ( y2,-885 ) );

   x1 =  -1072;
  y1 =  -1746;
  x2 =-545.5;
  y2 = -328.5;
 assert ( clipLine (  -604, -486, -370, 144,   &x1, &y1, &x2, &y2)
 && almostEqual ( x1,-604)
           && almostEqual ( y1, -486)
           && almostEqual ( x2, -545.5)
           && almostEqual ( y2, -328.5 ) );


   x1 =   -450;
  y1 =  -386.25;
  x2 =  -904;
  y2 =   -978 ;
 assert ( clipLine (  -450, -452, -223, -189,   &x1, &y1, &x2, &y2)
 && almostEqual ( x1,-604)
           && almostEqual ( y1, -486)
           && almostEqual ( x2, -545.5)
           && almostEqual ( y2, -328.5 ) );

  x1 =   0;
  y1 =  0.8448;
  x2 =  3.685 ;
  y2 =   0 ;
 assert ( clipLine (   0.335, 0.768, 1, 1,  &x1, &y1, &x2, &y2)
 && almostEqual ( x1,0.335)
           && almostEqual ( y1,0.768)
           && almostEqual ( x2,0.335)
           && almostEqual ( y2 ,0.768));

  return 0;


}
#endif /* __PROGTEST__ */
