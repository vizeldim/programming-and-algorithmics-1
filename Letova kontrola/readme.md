Úkolem je vytvořit program, který pomůže obsluze radaru řídící věže.

Při řízení letového provozu je důležité hlídat potenciální kolize letadel. Radary snímají pozice letadel a program kontroluje vzdálenosti mezi letadly. Chceme realizovat program, který pro zadané souřadnice letadel rozhodne, kde hrozí potenciální kolize, tedy která letadla jsou k sobě nejblíže.

Vstupem program jsou souřadnice letadel. Pro jednoduchost předpokládáme, že souřadnice letadle jsou rovinné, tedy pozice letadla je určena dvojicí čísel x a y. Souřadnice mají podobu desetinných čísel. Za souřadnicí následuje název letu, ten je tvořen řetězcem libovolné délky, název je ukončen odřádkováním. Tedy na každé řádce na vstupu jsou umístěné informace o právě jednom letu. Takto může být na vstupu zadáno velmi mnoho letů, jejich počet není dopředu známý. Zadávání letů končí s aktivním koncem souboru (EOF na stdin). Formát vstupu je zřejmý z ukázek níže.

Výstupem program je vzdálenost dvojice nejbližších letadel. V této (nejmenší) vzdálenosti může být více letadel najednou. Program proto vypíše všechny dvojice letů, které jsou od sebe vzdálené právě tuto nejmenší vzdálenost.

Program musí ošetřovat vstupní data. Pokud jsou vstupní data nesprávná, program to zjistí, zobrazí chybové hlášení a ukončí se. Za chybu je považováno:

nečíselné souřadnice,
chybějící/přebývající hranaté závorky nebo čárka,
méně než dva lety na vstupu (jinak není definovaná nejmenší vzdálenost).
Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti. V závislosti na zvoleném algoritmu může být úloha výpočetně náročnější. Správná implementace naivního algoritmu projde všemi testy kromě testu bonusového, tedy má šanci získat nominálních 100% bodů. Pro zvládnutí bonusového testu je potřeba použít efektivnější algoritmus, který dokáže v krátkém čase zvládnout větší objem testovacích dat (velké množství letů). Načtené lety si zřejmě budete muset ukládat do paměti. Je nutné použít dynamickou alokaci paměti, protože názvy letů a počty letů mohou být velmi různorodé. Testovací prostředí navíc pro malé vstupy citelně omezuje velikost dostupné paměti.

Ukázka práce programu:
Zadejte lety:
[0,0] KLM KL1981
[5, 0] Etihad Airways ETD26
[10, 0] British Airways BA123
[7, 0] Emirates UAE93P
[ 2 , 0 ] Wizz Air W67868
Nejmensi vzdalenost: 2.000000
KLM KL1981 - Wizz Air W67868
Etihad Airways ETD26 - Emirates UAE93P
Zadejte lety:
[0,5] Lufthansa LH948
[5,0] British Airways BA164
[0,0] Rynair FR2861
[5,5] Air France AF1886
[2.5,2.5] Korean Air KAL902
Nejmensi vzdalenost: 3.535534
Lufthansa LH948 - Korean Air KAL902
Rynair FR2861 - Korean Air KAL902
Korean Air KAL902 - British Airways BA164
Korean Air KAL902 - Air France AF1886
Zadejte lety:
[-10,-5] Air Malta AMC103
[10,0] easyJet EZY8732
[12,12] SAS SAS534
Nejmensi vzdalenost: 12.165525
easyJet EZY8732 - SAS SAS534
Zadejte lety:
[-1000000,0] Pegasus Airlines PGT1163
[1000000,0] Austrian Airlines AUA849
[5000000,0] Air India AIC142
Nejmensi vzdalenost: 2000000.000000
Pegasus Airlines PGT1163 - Austrian Airlines AUA849
Zadejte lety:
[10,10] Air Progtest
[10,10] PA1 Airways
[20, 20] PA2 Airways
[20,20] Segmentation Faultways
[20,20] PS1 sHellways
[10,10] AAG Wings
Nejmensi vzdalenost: 0.000000
Air Progtest - PA1 Airways
Air Progtest - AAG Wings
PA1 Airways - AAG Wings
PA2 Airways - Segmentation Faultways
PA2 Airways - PS1 sHellways
Segmentation Faultways - PS1 sHellways
Zadejte lety:
[3,abc] Air Progtest
Nespravny vstup.
Zadejte lety:
[0,0] Air Progtest
[5,8 PA1 Airways
[10,10] Segmentation Faultways
Nespravny vstup.

